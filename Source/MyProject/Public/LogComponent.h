// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "LogComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECT_API ULogComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULogComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Writes message to a .csv file of name specified in details
	UFUNCTION(BlueprintCallable, Category = Logging)
	void LogToFile(FString Message);

	// Log file name
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Setup)
	FString LogFileName;
	
};
