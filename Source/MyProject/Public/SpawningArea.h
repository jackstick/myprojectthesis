// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "SpawningArea.generated.h"

/**
 * 
 */
//Forward declaration
class AShape;

UCLASS()
class MYPROJECT_API USpawningArea : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
protected:
	//UPROPERTY(EditAnywhere, Category = Spawning)
	//UStaticMesh* WhatToSpawn;
	UPROPERTY(EditAnywhere, Category = Spawning)
	TSubclassOf<class AShape> WhatToSpawn;

	UPROPERTY(EditAnywhere, Category = "Spawning")
	int NumberToSpawn = 100;
private:
	//Returns a random point in volume
	FVector GetRandomPointInOwner() const;

	void SpawnShape();

	FRotator GetRandomRotation();
	
	
};
