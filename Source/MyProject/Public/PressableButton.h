// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PressableButton.generated.h"

class UButtonSwitch;
class USpawnComponent;
//class AShape;

UCLASS()
class MYPROJECT_API APressableButton : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APressableButton();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = Setup)
	void SetButtonReference(UStaticMeshComponent* ButtonSwitchToSet);


	UStaticMeshComponent* ButtonSwitch = nullptr;

protected:
	UPROPERTY(VisibleAnywhere)
	USpawnComponent* SpawnComponent = nullptr;

//	UPROPERTY(VisibleAnywhere)
//	AShape* Shape = nullptr;
	
};
