// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "SpawnComponent.generated.h"

class AShape;

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EGenderEnum : uint8
{
	VE_Female 	UMETA(DisplayName = "Female"),
	VE_Male 	UMETA(DisplayName = "Male")
};

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ESpawnMethodEnum : uint8
{
	VE_AllSectors 	UMETA(SpawnMethod = "All Sectors"),
	VE_OneSector 	UMETA(SpawnMethod = "One Sector")
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECT_API USpawnComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USpawnComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;


	// Log file name
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Setup)
	FString LogFileName;
	// Subject's id number
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Participants Information", meta = (ClampMin = "0"))
	int SubjectNumber;
	// Subject's gender
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Participants Information")
	EGenderEnum SubjectssGender;
	// Subject's age
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Participants Information", meta = (ClampMin = "0", ClampMax = "100"))
	int SubjectsAge;
	// Spawning method
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Setup)
	ESpawnMethodEnum SpawnMethod;
	// Trial number
	UPROPERTY(BlueprintReadOnly, Category = Experiment)
	int TrialNumber = 0;


	// Initial spawning of all shapes
	UFUNCTION(BlueprintCallable, Category = Spawning)
	void SpawnAllSectors();

	// Spawns the new extra sector, by changing the shapes status to active
	UFUNCTION(BlueprintCallable, Category = Spawning)
	void SpawnExtraSector();

	// Despawns the currently active extra sector, by changing the shapes status to despawning
	UFUNCTION(BlueprintCallable, Category = Spawning)
	void DespawnCurrentExtraSector();

	// The current salient side
	UPROPERTY(BlueprintReadOnly, Category = Spawning)
	int SalientSide = NULL;

	// Returns reference to all of the shapes considered as extra shapes
	UFUNCTION(BlueprintCallable, Category = Spawning)
	TArray<AShape*> GetExtraSectionShapes(int SectorIndex);

	// Appends data to the log message map
	UFUNCTION(BlueprintCallable, Category = Logging)
	void AppendToLogMessage(int Key, FString Message);

	// Writes log file header to the log file
	UFUNCTION(BlueprintCallable, Category = Logging)
	void LogFileHeader();

	// Logs the completed message to the log file
	UFUNCTION(BlueprintCallable, Category = Logging)
	void LogToFile();

	// Logs the failed attempt to the log file
	UFUNCTION(BlueprintCallable, Category = Logging)
	void LogFailedAttempt();

	
protected:
	// A shape to spawn 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Setup)
	TSubclassOf<AShape> WhatToSpawn;

	// Number of shapes to spawn per sector
	UPROPERTY(EditAnywhere, Category = Setup)
	int NumberToSpawn = 150;

	// Radius of the half sphere on which shapes are spawned
	UPROPERTY(EditAnywhere, Category = Setup)
	float RingRadius = 50.f;

	UPROPERTY(EditAnywhere, Category = Setup)
	float SpawnDelay = 1.f;

	// Number of total sectors
	int NumberOfSectors = 4;

private:
	// Returns a random rotation for spawning a shape
	FRotator GetRandomRotation();

	// Random number generator
	float RandomNumberInRange(float min, float max);

	// Returns a random point on a surface of a sphere with a set radius
	FVector GetRandomSpherePoint(float Radius);

	//Spawns a single shape to a set location with a random rotation
	AShape* SpawnShapeToLocation(FVector SpawnLocation);

	//Spawns shapes inside a sector of the sphere, currently quarter 
	/*Sectors are ordered as follows	2|1
										3|4 */
	void SpawnSector(int SectorNumber, float SpawnNumberCoefficient, bool IsExtraSector = false);

	FString GetGenderEnumAsString(EGenderEnum EnumValue);
	FString GetSpawnMethodEnumAsString(ESpawnMethodEnum EnumValue);
	
	void PopulateExtraShapeArray(int SectionNumber, AShape* Shape);

	// Holds log message index and the corresponding message
	TMap<int, FString> LogMessage;

	int RandomSectorIndexGenerator();

	// Map to hold sector number and its corresponding extra shape references
	TMap<int, TArray<AShape*>> ExtraSectionShapes;







	
	
};
