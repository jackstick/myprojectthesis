// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Shape.generated.h"

class UStaticMeshComponent;

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EShapeScaleEnum : uint8
{
	VE_Active 	UMETA(DisplayName = "Active"),
	VE_Despawning 	UMETA(DisplayName = "Despawning"),
	VE_Despawned	UMETA(DisplayName = "Despawned")
};

UCLASS()
class MYPROJECT_API AShape : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShape();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	int GetSelectedMeshIndex() const;

	void SetSelectedMeshIndex(int IndexToBe);

	UFUNCTION(BlueprintCallable, Category = Setup)
	int GetNumberOfPossibleMeshes() const;

	UFUNCTION(BlueprintCallable, Category = Setup)
	void SetNumberOfPossibleMeshes(int MaxNumberToBe);

	// Scale limits and scaling speed
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Scaling)
	float MaximumScale = 0.03f;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Scaling)
	float MinimumScale = 0.f;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Scaling)
	float ScalingSpeed = 0.001f;

	// Function used to pulsate the shape
	UFUNCTION(BlueprintCallable, Category = Scaling)
	void PulsateScale(float MinScale, float MaxScale, float ScaleSpeed);

	// Function used to scale the shape to 0
	void ScaleToNothing();

	void SetScalingPhase(EShapeScaleEnum NewValue);

	void SetIsExtraShape(bool NewValue);

	UFUNCTION(BlueprintCallable, Category = Setup)
	bool GetIsExtraShape();

	

private:

	// Referece to the mesh component used for shapes
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shape", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* ShapeMesh;

	//UPROPERTY(EditAnywhere, Category = Setup, meta = (ClampMin = "0.0", ClampMax = "20.0"))
	int NumberOfPossibleMeshes = 0;

	// Used in "All Sectors" spawn method to change between different meshes
	int SelectedMeshIndex = 0;

	// Variable to keep track of if shape is scaling up or down
	bool ScaleUp;

	// Scaling phase enum: Active - shapes is constantly pulsating; Despawning - shapes is being scaled to zero; Despawned - shape is inactive and not pulsating
	EShapeScaleEnum ScalingPhase = EShapeScaleEnum::VE_Active;

	// Is the shape considered to be an extra shapes
	bool IsExtraShape = false;


	
};
