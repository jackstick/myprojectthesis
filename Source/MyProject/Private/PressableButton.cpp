// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "SpawnComponent.h"
#include "Shape.h"
#include "LogComponent.h"
#include "Shape.h"
#include "PressableButton.h"


// Sets default values
APressableButton::APressableButton()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SpawnComponent = CreateDefaultSubobject<USpawnComponent>(FName("Spawning Component"));
	//Shape = CreateDefaultSubobject<AShape>(FName("Shape"));
}

// Called when the game starts or when spawned
void APressableButton::BeginPlay()
{
	Super::BeginPlay();


	if (ButtonSwitch)
	{
		FVector ButtonLocation = ButtonSwitch->GetComponentLocation();
		//UE_LOG(LogTemp, Warning, TEXT("Switch location: %s"), *ButtonLocation.ToString())
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Null pointer reference"))
	}
	//AShape Shape;
	//UE_LOG(LogTemp, Warning, TEXT("Shape info: %i"), Shape.GetNumberOfPossibleMeshes())
	
}

// Called every frame
void APressableButton::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void APressableButton::SetButtonReference(UStaticMeshComponent* ButtonSwitchToSet)
{
	ButtonSwitch = ButtonSwitchToSet;
}



