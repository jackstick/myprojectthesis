// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Shape.h"
#include <random>
#include <fstream>
#include <iterator>
#include <map>
#include "LogComponent.h"
#include "SpawnComponent.h"



// Sets default values for this component's properties
USpawnComponent::USpawnComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void USpawnComponent::BeginPlay()
{

	//FTimerHandle UnusedHandle;
	//GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &USpawnComponent::SpawnAllSectors, SpawnDelay, false);
	Super::BeginPlay();
	
	// Testing random number distribution
	/*
	std::map<int, int> m;
	for (int i = 0; i < 100000; i++)
	{
		++m[RandomSectorIndexGenerator()];
	}

	for (auto p : m)
	{
		UE_LOG(LogTemp, Warning, TEXT("%i generated %i times"), p.first, p.second)
	}
	*/
		
	//SpawnAllSectors();
	//UE_LOG(LogTemp, Warning, TEXT("%s"), *(FPaths::GetProjectFilePath()))
		//FTimerHandle UnusedHandle2;
		//GetWorld()->GetTimerManager().SetTimer(UnusedHandle2, this, &USpawnComponent::SpawnExtraSector, SpawnDelay+1.f, false);

	//LogToFile(FString("Date;Subject's Number;Gender;Age;Trial Number;Spawn Method;Dense Sector;Button Colour;Hand Used;Wrong Start Detected;Wrong Hand Used;Hand Returned To Initial Location;Reaction Time Too Fast;Reaction Time Too Slow;Stimuli Start Time;Button Press Time;Reaction Time;\n"));

	
}


// Called every frame
void USpawnComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );
	//float CurrentTime = GetWorld()->GetTimeSeconds();
	//UE_LOG(LogTemp, Warning, TEXT("Current time: %f"), CurrentTime)
}

void USpawnComponent::SpawnAllSectors()
{
	// Different Spawn Methods
	//UE_LOG(LogTemp, Warning, TEXT("Spawn Method: %s"), *(GetSpawnMethodEnumAsString(SpawnMethod)))
	if (SpawnMethod == ESpawnMethodEnum::VE_AllSectors)
	{
		//int DenseSector = 1 + (rand() % (int)(4 - 1 + 1));
		// Individual sectors
		//int DenseSector = RandomSectorIndexGenerator();
		SalientSide = FMath::RandRange(1, 2);

		if (SalientSide == 1)
		{
			for (int i = 1; i <= 4; i++)
			{
				if (i == 1 || i == 3) {
					SpawnSector(i, 2.f);
				}
				else {
					SpawnSector(i, 1.f);
				}
			}
		}
		else if (SalientSide == 2)
		{
			for (int i = 1; i <= 4; i++)
			{
				if (i == 2 || i == 4) {
					SpawnSector(i, 2.f);
				}
				else {
					SpawnSector(i, 1.f);
				}
			}
		}

		// Current DateTime
		FString CurrentDate = FDateTime::Now().ToString();
		AppendToLogMessage(0, CurrentDate);
		// Subject's Number
		AppendToLogMessage(1, FString::FromInt(SubjectNumber));
		// Gender
		AppendToLogMessage(2, GetGenderEnumAsString(SubjectssGender));
		// Age
		AppendToLogMessage(3, FString::FromInt(SubjectsAge));
		// Trial Number
		TrialNumber += 1;
		AppendToLogMessage(4, FString::FromInt(TrialNumber));
		//Spawn Method
		AppendToLogMessage(5, GetSpawnMethodEnumAsString(SpawnMethod));
		// Dense Sector
		AppendToLogMessage(6, FString::FromInt(SalientSide));
		// Shape Spawn Time
		//float CurrentTime = GetWorld()->GetTimeSeconds();
		//AppendToLogMessage(14, FString::SanitizeFloat(CurrentTime));
	}
	else if(SpawnMethod == ESpawnMethodEnum::VE_OneSector)
	{
		for (int i = 1; i <= 4; i++)
		{
			SpawnSector(i, 1.f);
			SpawnSector(i, 2.f, true);
			
			if (ExtraSectionShapes.Num() == 4)
			{
				//UE_LOG(LogTemp, Warning, TEXT("%i | %i | %i | %i | %i"), ExtraSectionShapes.Num(), ExtraSectionShapes.Find(1)->Num(), ExtraSectionShapes.Find(2)->Num(), ExtraSectionShapes.Find(3)->Num(), ExtraSectionShapes.Find(4)->Num())
			}
		}
	}
	
	return;
}

TArray<AShape*> USpawnComponent::GetExtraSectionShapes(int SectorIndex)
{
	if(ExtraSectionShapes.Num() == 4)
	{
		//UE_LOG(LogTemp, Warning, TEXT("EXTRA: %i"), ExtraSectionShapes.Find(SectorIndex)->Num())
		return *(ExtraSectionShapes.Find(SectorIndex));
	}
	return {};
}

void USpawnComponent::SpawnExtraSector()
{
	if(ExtraSectionShapes.Num() > 0)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Here %i | %i | %i | %i | %i"), ExtraSectionShapes.Num(), ExtraSectionShapes.Find(1)->Num(), ExtraSectionShapes.Find(2)->Num(), ExtraSectionShapes.Find(3)->Num(), ExtraSectionShapes.Find(4)->Num())
		// Despawn extra sector
		DespawnCurrentExtraSector();
		//CurrentExtraSectorIndex = 1 + (rand() % (int)(4 - 1 + 1));
		
		//Old version of 50:50 distribution between sides
		//SalientSide = FMath::RandRange(1, 2);
		SalientSide = RandomSectorIndexGenerator();

		// Current DateTime
		FString CurrentDate = FDateTime::Now().ToString();
		AppendToLogMessage(0, CurrentDate);
		// Subject's Number
		AppendToLogMessage(1, FString::FromInt(SubjectNumber));
		// Gender
		AppendToLogMessage(2, GetGenderEnumAsString(SubjectssGender));
		// Age
		AppendToLogMessage(3, FString::FromInt(SubjectsAge));
		// Trial Number
		TrialNumber += 1;
		AppendToLogMessage(4, FString::FromInt(TrialNumber));
		//Spawn Method
		AppendToLogMessage(5, GetSpawnMethodEnumAsString(SpawnMethod));
		// Dense Sector
		AppendToLogMessage(6, FString::FromInt(SalientSide));

		if (SalientSide == 1) 
		{
			for (AShape* element : ExtraSectionShapes.FindRef(1))
			{
				element->SetActorHiddenInGame(false);
				element->SetScalingPhase(EShapeScaleEnum::VE_Active);
			}
			for (AShape* element : ExtraSectionShapes.FindRef(3))
			{
				element->SetActorHiddenInGame(false);
				element->SetScalingPhase(EShapeScaleEnum::VE_Active);
			}
		}
		else if (SalientSide == 2)
		{
			for (AShape* element : ExtraSectionShapes.FindRef(2))
			{
				element->SetActorHiddenInGame(false);
				element->SetScalingPhase(EShapeScaleEnum::VE_Active);
			}
			for (AShape* element : ExtraSectionShapes.FindRef(4))
			{
				element->SetActorHiddenInGame(false);
				element->SetScalingPhase(EShapeScaleEnum::VE_Active);
			}
		}
		
	}
	return;
}

void USpawnComponent::DespawnCurrentExtraSector()
{
	if (SalientSide != NULL)
	{
		if (SalientSide == 1)
		{
			for (AShape* element : ExtraSectionShapes.FindRef(1))
			{
				element->SetScalingPhase(EShapeScaleEnum::VE_Despawning);
			}
			for (AShape* element : ExtraSectionShapes.FindRef(3))
			{
				element->SetScalingPhase(EShapeScaleEnum::VE_Despawning);
			}
		}
		else if (SalientSide == 2)
		{
			for (AShape* element : ExtraSectionShapes.FindRef(2))
			{
				element->SetScalingPhase(EShapeScaleEnum::VE_Despawning);
			}
			for (AShape* element : ExtraSectionShapes.FindRef(4))
			{
				element->SetScalingPhase(EShapeScaleEnum::VE_Despawning);
			}
		}
	}
}

int USpawnComponent::RandomSectorIndexGenerator()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	// Sector index is weighed for the bottom two at 35% each and the top two at 15% each
	double weights[] = { 0.66, 0.17, 0.17};
	std::discrete_distribution<> d(std::begin(weights), std::end(weights));
	return d(gen);
}

FRotator USpawnComponent::GetRandomRotation()
{
	FRotator SpawnRotaton;
	SpawnRotaton.Yaw = FMath::FRand() * 360.0f;
	SpawnRotaton.Pitch = FMath::FRand() * 360.0f;
	SpawnRotaton.Roll = FMath::FRand() * 360.0f;
	return SpawnRotaton;
}

float USpawnComponent::RandomNumberInRange(float min, float max)
{
	std::random_device rd;  
	std::mt19937 gen(rd()); 
	std::uniform_real_distribution<> dis(min, max);
	return dis(gen);
}

FVector USpawnComponent::GetRandomSpherePoint(float Radius)
{
	float XCoord = RandomNumberInRange(0.5, 15) - 0.5;
	float YCoord = RandomNumberInRange(0.5, 15) - 0.5;
	float ZCoord = RandomNumberInRange(0.5, 15) - 0.5;

	FVector OutPut = FVector(XCoord, YCoord, ZCoord);

	float Norm = pow(pow(abs(XCoord), 2) + pow(abs(YCoord), 2) + pow(abs(ZCoord), 2), 0.5);
	
	return (OutPut/Norm)*Radius;
}

AShape* USpawnComponent::SpawnShapeToLocation(FVector SpawnLocation)
{
	AShape* SpawnedShape = nullptr;
	if (WhatToSpawn != NULL)
	{
		FRotator SpawnRotation = GetRandomRotation();
		SpawnedShape = (GetWorld()->SpawnActor<AShape>(WhatToSpawn, SpawnLocation, SpawnRotation));
	}
	return SpawnedShape;

}

void USpawnComponent::SpawnSector(int SectorNumber, float SpawnNumberCoefficient, bool IsExtraSector)
{
	FVector ButtonLocation = GetOwner()->GetActorLocation();

	switch(SectorNumber)
	{
		case 1:
			for (int i = 0; i < NumberToSpawn*SpawnNumberCoefficient; i++)
			{
				// Generate a sphere surface vector
				FVector SphereCoord = GetRandomSpherePoint(RingRadius) * -1;
				// Change the Y coordinate to offset the sphere from the button
				SphereCoord.Y = SphereCoord.Y + RingRadius - 3;
				// Spawn all 4 sectors of the sphere by changing the X and Z coordinate signs
				SphereCoord.X = (SphereCoord.X * -1);
				SphereCoord.Z = (SphereCoord.Z * -1);
				FVector SpawnLocation = ButtonLocation + SphereCoord;
				// Spawn the shape
				//AShape SpawnedShape;
				AShape* SpawnedShape = SpawnShapeToLocation(SpawnLocation);

				if(IsExtraSector)
				{
					PopulateExtraShapeArray(1, SpawnedShape);
				}
			}
			break;
		case 2:
			for (int i = 0; i < NumberToSpawn*SpawnNumberCoefficient; i++)
			{
				FVector SphereCoord = GetRandomSpherePoint(RingRadius) * -1;
				SphereCoord.Y = SphereCoord.Y + RingRadius - 3;
				SphereCoord.Z = (SphereCoord.Z * -1);
				FVector SpawnLocation = ButtonLocation + SphereCoord;
				//AShape SpawnedShape;
				
				AShape* SpawnedShape = SpawnShapeToLocation(SpawnLocation);

				if (IsExtraSector)
				{
					PopulateExtraShapeArray(2, SpawnedShape);
				}
			}
			break;
		case 3:
			for (int i = 0; i < NumberToSpawn*SpawnNumberCoefficient; i++)
			{
				FVector SphereCoord = GetRandomSpherePoint(RingRadius) * -1;
				SphereCoord.Y = SphereCoord.Y + RingRadius - 3;
				FVector SpawnLocation = ButtonLocation + SphereCoord;

				AShape* SpawnedShape = SpawnShapeToLocation(SpawnLocation);

				if (IsExtraSector)
				{
					PopulateExtraShapeArray(3, SpawnedShape);
				}

			}
			break;
		case 4:
			for (int i = 0; i < NumberToSpawn*SpawnNumberCoefficient; i++)
			{
				FVector SphereCoord = GetRandomSpherePoint(RingRadius) * -1;
				SphereCoord.Y = SphereCoord.Y + RingRadius - 3;
				SphereCoord.X = (SphereCoord.X * -1);
				FVector SpawnLocation = ButtonLocation + SphereCoord;
				//AShape SpawnedShape;
				AShape* SpawnedShape = SpawnShapeToLocation(SpawnLocation);
				/*if (SpawnedShape)
				{
					SpawnedShape->SetActorHiddenInGame(true);
				}*/
				if (IsExtraSector)
				{
					PopulateExtraShapeArray(4, SpawnedShape);
				}
			}
			break;
		default:
			UE_LOG(LogTemp, Warning, TEXT("Invalid sector number was entered!"))
			return;
	}
}

void USpawnComponent::PopulateExtraShapeArray(int SectionNumber, AShape* Shape)
{
	if (Shape)
	{
		Shape->SetActorHiddenInGame(true);
		//Shape->SetIsExtraShape(true);
		Shape->SetScalingPhase(EShapeScaleEnum::VE_Despawned);
		Shape->SetActorScale3D(FVector(0));
		ExtraSectionShapes.FindOrAdd(SectionNumber).Add(Shape);
	}

}

void USpawnComponent::LogFileHeader()
{
	if (!LogFileName.IsEmpty())
	{
		FString HeaderText = FString("Date;Subject's Number;Gender;Age;Trial Number;Spawn Method;Salient Side;Button Colour;Hand Used;Wrong Start Detected;Wrong Hand Used;Reaction Time Too Fast;Reaction Time Too Slow;Shape Spawn Delay;Stimulus Delay;Stimuli Start Time;Button Press Time;Reaction Time;\n");
		FString AbsolutePath = FPaths::GetPath(FPaths::GetProjectFilePath()).Append(FString("/Logs/"));
		AbsolutePath.Append(LogFileName);
		AbsolutePath.Append(FString(".csv"));
		
		//FString TempLogFileName = LogFileName.Append(FString(".csv"));
		UE_LOG(LogTemp, Warning, TEXT("Filename: %s; Message: %s"), *(LogFileName), *HeaderText)

		std::ofstream OutFile;
		OutFile.open(*AbsolutePath, std::ios_base::app);
		OutFile << TCHAR_TO_ANSI(*HeaderText);
		OutFile.close();
		return;
	}
}

void USpawnComponent::AppendToLogMessage(int Key, FString Message)
{
	if (!LogFileName.IsEmpty())
	{
		LogMessage.Add(Key, Message);
	}
	return;
}

void USpawnComponent::LogToFile()
{
	if (!LogFileName.IsEmpty())
	{
		FString AbsolutePath = FPaths::GetPath(FPaths::GetProjectFilePath()).Append(FString("/Logs/"));
		
		AbsolutePath.Append(LogFileName);
		AbsolutePath.Append(FString(".csv"));
		FString AppendedMessage;
		for(int i=0;i<=17;i++)
		{
			if(LogMessage.Contains(i))
			{
				AppendedMessage.Append(*(LogMessage.Find(i)));			
			}
			AppendedMessage.Append(FString(";"));
		}
		//FString TempLogFileName = LogFileName.Append(FString(".csv"));
		UE_LOG(LogTemp, Warning, TEXT("Filename: %s; Message: %s"), *(LogFileName), *AppendedMessage)
		AppendedMessage.Append(FString("\n"));
		LogMessage.Empty();
		
		std::ofstream OutFile;
		OutFile.open(*AbsolutePath, std::ios_base::app);
		OutFile << TCHAR_TO_ANSI(*AppendedMessage);
		OutFile.close();
		return;
	}
	else
	{
		return;
		//UE_LOG(LogTemp, Warning, TEXT("Please set a log filename!"))
	}

}

void USpawnComponent::LogFailedAttempt()
{
	TrialNumber -= 1;
	for (int i = 7; i <= 16; i++)
	{
		AppendToLogMessage(i, FString("NaN"));
	}
	LogToFile();
}


FString USpawnComponent::GetGenderEnumAsString(EGenderEnum EnumValue)
{
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EGenderEnum"), true);
	if (!EnumPtr) return FString("Invalid");

	return EnumPtr->GetEnumName((int32)EnumValue); // for EnumValue == VE_Dance returns "VE_Dance"
}

FString USpawnComponent::GetSpawnMethodEnumAsString(ESpawnMethodEnum EnumValue)
{
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("ESpawnMethodEnum"), true);
	if (!EnumPtr) return FString("Invalid");

	return EnumPtr->GetEnumName((int32)EnumValue); // for EnumValue == VE_Dance returns "VE_Dance"
}


