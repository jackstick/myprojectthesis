// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Shape.h"


// Sets default values
AShape::AShape()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	ShapeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShapeMesh"));
	RootComponent = ShapeMesh;
	ScaleUp = FMath::RandBool();

}

// Called when the game starts or when spawned
void AShape::BeginPlay()
{
	Super::BeginPlay();
	
	//UE_LOG(LogTemp, Warning, TEXT("%f; %f; %f"), MinimumScale, MaximumScale, ScalingSpeed)
	
}

// Called every frame
void AShape::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	if(ScalingPhase == EShapeScaleEnum::VE_Active)
	{
		PulsateScale(MinimumScale, MaximumScale, ScalingSpeed);
	}
	else if(ScalingPhase == EShapeScaleEnum::VE_Despawning)
	{
		ScaleToNothing();
	}

}

int AShape::GetSelectedMeshIndex() const
{
	return SelectedMeshIndex;
}

void AShape::SetSelectedMeshIndex(int IndexToBe)
{
	SelectedMeshIndex = IndexToBe;
}

int AShape::GetNumberOfPossibleMeshes() const
{
	return NumberOfPossibleMeshes;
}

void AShape::SetNumberOfPossibleMeshes(int MaxNumberToBe)
{
	NumberOfPossibleMeshes = MaxNumberToBe;
}

void AShape::PulsateScale(float MinScale, float MaxScale, float ScaleSpeed)
{
	//float Time = GetGameTimeSinceCreation();
	FVector CurrentScale = this->GetActorScale();
	//UE_LOG(LogTemp, Warning, TEXT("%f: %d; %s"), Time, ScaleUp, *CurrentScale.ToString())
	FVector NewScale;
	if(CurrentScale.X >= MaxScale)
	{
		ScaleUp = false;
	}
	else if(CurrentScale.X <= MinScale)
	{
		ScaleUp = true;
	}

	if(ScaleUp)
	{
		NewScale = CurrentScale + ScaleSpeed;
	}
	else
	{
		NewScale = CurrentScale - ScaleSpeed;
	}
	this->SetActorScale3D(NewScale);

}

void AShape::ScaleToNothing()
{	
	FVector CurrentScale = this->GetActorScale();
	if(CurrentScale.X > 0)
	{
		FVector NewScale = CurrentScale - ScalingSpeed;
		//UE_LOG(LogTemp, Warning, TEXT("%s"), )
		this->SetActorScale3D(NewScale);
	}
	else
	{
		this->SetActorHiddenInGame(true);
		ScalingPhase = EShapeScaleEnum::VE_Despawned;
	}
}

void AShape::SetScalingPhase(EShapeScaleEnum NewValue)
{
	ScalingPhase = NewValue;
}

void AShape::SetIsExtraShape(bool NewValue)
{
	IsExtraShape = NewValue;
}

bool AShape::GetIsExtraShape()
{
	return IsExtraShape;
}




