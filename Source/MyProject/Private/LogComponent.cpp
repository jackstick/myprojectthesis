// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include <fstream>
#include <iostream>
#include <string>
#include "LogComponent.h"


// Sets default values for this component's properties
ULogComponent::ULogComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void ULogComponent::BeginPlay()
{
	Super::BeginPlay();

	//TODO: Write log file header
	//FString Message("Namee;");
	//LogToFile(LogFileName, Message);
	
}

void ULogComponent::LogToFile(FString Message)
{
	FString AbsolutePath = FString("C:/Users/Taavi/Desktop/");
	AbsolutePath.Append(LogFileName);
	AbsolutePath.Append(FString(".csv"));

	std::ofstream OutFile;
	//TODO: Fix this absolute path

	OutFile.open(*AbsolutePath, std::ios_base::app);
	OutFile << TCHAR_TO_ANSI(*Message);
	OutFile.close();
	UE_LOG(LogTemp, Warning, TEXT("Logging method was called, with filename: %s and message: %s"), *LogFileName, *Message);
	return;
}


