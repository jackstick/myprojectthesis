// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Shape.h"
#include "SpawningArea.h"
#include "Kismet/KismetMathLibrary.h"




void USpawningArea::BeginPlay()
{
	Super::BeginPlay();

	GetRandomPointInOwner();

	for (int i = 0; i < NumberToSpawn; i++)
	{
		SpawnShape();
	}

	FVector LocationInWorld = FVector(3325.0, 1300.0, 300.0);
	FRotator Rotation = GetRandomRotation();


}

FVector USpawningArea::GetRandomPointInOwner() const
{
	FVector Origin;
	FVector BoxExtent;
	GetOwner()->GetActorBounds(true, Origin, BoxExtent);

	FVector CollisionExtent = GetOwner()->GetSimpleCollisionCylinderExtent();

	FVector RandomPoint = UKismetMathLibrary::RandomPointInBoundingBox(Origin, BoxExtent);
	FVector OutParam;
	float distance = UPrimitiveComponent::GetDistanceToCollision(RandomPoint, OutParam);
	//UE_LOG(LogTemp, Warning, TEXT("Distance: %f, random point: %s and out point: %s"), distance, *RandomPoint.ToString(), *OutParam.ToString())
	//UE_LOG(LogTemp, Warning, TEXT("%s ; %s ; %s"), *Origin.ToString(), *BoxExtent.ToString(), *RandomPoint.ToString())

	return RandomPoint;
}

FRotator USpawningArea::GetRandomRotation()
{

	FRotator SpawnRotaton;
	SpawnRotaton.Yaw = FMath::FRand() * 360.0f;
	SpawnRotaton.Pitch = FMath::FRand() * 360.0f;
	SpawnRotaton.Roll = FMath::FRand() * 360.0f;
	return SpawnRotaton;

}

void USpawningArea::SpawnShape()
{
	if (WhatToSpawn != NULL)
	{
		UWorld* const World = GetWorld();
		if (World)
		{
			FVector SpawnLocation = GetRandomPointInOwner();
			FRotator SpawnRotation = GetRandomRotation();
			
			AShape* const SpawnedPickup = World->SpawnActor<AShape>(WhatToSpawn, SpawnLocation, SpawnRotation);
			float Radius = 100.f;


			for (int i = 0; i < 360; i++)
			{

			}
			//SpawnDelay = FMath::FRandRange(SpawnDelayRangeLow, SpawnDelayRangeHigh);
			//GetWorldTimerManager().SetTimer(SpawnTimer, this, &ASpawnVolume::SpawnPickup, SpawnDelay, false);
			//FTimerHandle SpawnTimer;
			//World->GetTimerManager().SetTimer(SpawnTimer, this, &USpawningArea::SpawnShape, 1.0, false);
		}

	}
	return;
}


